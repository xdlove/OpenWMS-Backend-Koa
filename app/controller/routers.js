/**
 * All the routes are combined together here.
 */
import Router from "koa-router";
import CombineRouters from "koa-combine-routers";

import categoryRouter from "./category-routers";
import customerRouter from "./customer-routers";
import inboundRouter from "./inbound-routers";
import inventoryRouter from "./inventory-routers";
import outboundRouter from "./outbound-routers";
import staffRouter from "./staff-routers";
import vendorRouter from "./vendor-routers";
import warehouseRouter from "./warehouse-routers";

let defaultRouter = new Router();

defaultRouter.get("/", async (ctx, next) => {
  ctx.body = "首页";
});

const allRoutes = CombineRouters([
  categoryRouter,
  customerRouter,
  inboundRouter,
  inventoryRouter,
  outboundRouter,
  staffRouter,
  vendorRouter,
  warehouseRouter,
  defaultRouter
]);

export default allRoutes;
