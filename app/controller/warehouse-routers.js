import Router from "koa-router";
import warehouseService from "../service/warehouse-service";
import koaBody from "koa-body";
let router = new Router({
  prefix: "/warehouses"
});

router.get("/:id", async (ctx, next) => {
  const warehouse = warehouseService.getById(ctx.params.id);
  ctx.body = warehouse;
  next();
});

router.get("/", async (ctx, next) => {
  let warehouses = warehouseService.list();
  ctx.body = warehouses;
  next();
});

router.post("/", koaBody(), async (ctx, next) => {
  const res = warehouseService.add(ctx.request.body);
  ctx.body = res;
  next();
});

router.put("/:id", koaBody(), async (ctx, next) => {
  const id = ctx.params.id;
  const res = warehouseService.update(id, ctx.request.body);
  ctx.body = res;
  next();
});

router.delete("/:id", async (ctx, next) => {
  const id = ctx.params.id;
  const res = warehouseService.delete(id);
  ctx.body = res;
  next();
});

export default router;
