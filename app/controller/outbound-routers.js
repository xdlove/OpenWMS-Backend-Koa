import Router from "koa-router";
import OutboundService from "../service/outbound-service";

let router = new Router();

router.get("/outbound/getOutboundDetail", async (ctx, next) => {
  let outboundDetail=new OutboundService().getOutboundDetail();
  ctx.body = JSON.stringify(outboundDetail);
});

router.get("/outbound/getOutboundReceipt", async (ctx, next) => {
  let outboundReceipt=new OutboundService().getOutboundReceipt();
  ctx.body = JSON.stringify(outboundReceipt);
});

router.get("/outbound/getOutboundRecords", async (ctx, next) => {
  let outboundRecords=new OutboundService().getOutboundRecords();
  ctx.body = JSON.stringify(outboundRecords);
});

export default router;
