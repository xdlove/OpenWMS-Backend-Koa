import dbHelper from "../utils/better-db-helper";
import BaseService from "./BaseService";
class StaffService extends BaseService{
  emptyData = {
    staffCode: "",
    staffName: "",
    cellphone: "",
    qq: "",
    email: "",
    remark: "",
    gender: ""
  }

  getById(id) {
    return dbHelper.getById(`select * from user where id=@id`, {id});
  }
  list(params) {
    const {
      pageNumber,
      pageSize,
      staffCode,
      staffName,
      cellphone,
      qq,
      email,
      gender
    } = Object.assign({}, this.emptyData, this.pagination, params);

    const whereSegment = `where staff_code like @staffCode and staff_name like @staffName
      and cellphone like @cellphone and qq like @qq and email like @email and gender like @gender`;

    const sqlParams = {
      staffCode: `%${staffCode}%`,
      staffName: `%${staffName}%`,
      cellphone: `%${cellphone}%`,
      qq: `%${qq}%`,
      email: `%${email}%`,
      gender: `%${gender}%`
    };

    //total
    const total = dbHelper.count(`select count(*) from user ${whereSegment}`, sqlParams);
    //rows
    const rows = dbHelper.queryRows(`select 
      id, staff_code staffCode,
      staff_name staffName,cellphone,
      gender,qq,email,duty,remark
      from user ${whereSegment} limit @offset, @limit`,
    Object.assign(sqlParams, {
      offset: (pageNumber - 1) * pageSize,
      limit: pageSize
    }));
    return {
      rows,
      total
    };
  }

  add(params) {
    return dbHelper.insert("insert into user values(null, @staffCode, @staffName, @gender,@cellphone, @qq, @email,@duty,@remark)", Object.assign({}, this.emptyData, params));
  }

  update(id, params) {
    return dbHelper.update("update user set staff_code=@staffCode, staff_name=@staffName, gender=@gender, cellphone=@cellphone, qq=@qq, email=@email, duty=@duty, remark=@remark where id =@id", Object.assign({
      id
    }, this.emptyData, params));
  }

  delete(id) {
    return dbHelper.delete("delete from user where id = @id", {
      id
    });
  }
}
export default new StaffService;
