class BaseService {
  pagination = {
    pageSize: 2,
    pageNumber: 1
  }
}

export default BaseService;
